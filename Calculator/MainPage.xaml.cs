﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calculator
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    public partial class MainPage : ContentPage
    {
        string mathOp;

        int number1;
        int number2;
        int result;
        public MainPage()
        { 

            InitializeComponent();
            displayNumber_Label.Text = " ";

        }
        //Number Buttons first
        private void One_Button(object send, EventArgs l)
        {
            displayNumber_Label.Text = displayNumber_Label.Text + "1";
        }

        private void Two_Button(object send, EventArgs l)
        {
            displayNumber_Label.Text = displayNumber_Label.Text + "2";
        }

        private void Three_Button(object send, EventArgs l)
        {
            displayNumber_Label.Text = displayNumber_Label.Text + "3";
        }

        private void Four_Button(object send, EventArgs l)
        {
            displayNumber_Label.Text = displayNumber_Label.Text + "4";
        }

        private void Five_Button(object send, EventArgs l)
        {
            displayNumber_Label.Text = displayNumber_Label.Text + "5";
        }

        private void Six_Button(object send, EventArgs l)
        {
            displayNumber_Label.Text = displayNumber_Label.Text + "6";
        }

        private void Seven_Button(object send, EventArgs l)
        {
            displayNumber_Label.Text = displayNumber_Label.Text + "7";
        }

        private void Eight_Button(object send, EventArgs l)
        {
            displayNumber_Label.Text = displayNumber_Label.Text + "8";
        }

        private void Nine_Button(object send, EventArgs l)
        {
            displayNumber_Label.Text = displayNumber_Label.Text + "9";
        }

        private void Zero_Button(object send, EventArgs l)
        {
            displayNumber_Label.Text = displayNumber_Label.Text + "0";
        }

        //Math Operations Next
        private void Add_Button(object send, EventArgs l)
        {
            if(displayNumber_Label != null)
            {
                number1 = int.Parse(displayNumber_Label.Text);
                displayNumber_Label.Text = " ";
                mathOp = "add";
            }
        }

        private void Subtract_Button(object send, EventArgs l)
        {
            if(displayNumber_Label != null)
            {
                number1 = int.Parse(displayNumber_Label.Text);
                displayNumber_Label.Text = " ";
                mathOp = "subtract";
            }
        }

        private void Multiply_Button(object send, EventArgs l)
        {
            if(displayNumber_Label != null)
            {
                number1 = int.Parse(displayNumber_Label.Text);
                displayNumber_Label.Text = " ";
                mathOp = "multiply";
            }
        }

        private void Divide_Button(object send, EventArgs l)
        {
            if(displayNumber_Label != null)
            {
                number1 = int.Parse(displayNumber_Label.Text);
                displayNumber_Label.Text = " ";
                mathOp = "divide";
            }
        }

        private void Clear_Button(object send, EventArgs l)
        {
            if(displayNumber_Label != null)
            {
                displayNumber_Label.Text = " ";
            }
        }

        //Equal Button will use operation button  to get result
        //ToString could be useful here

        private void Equal_Button(object send, EventArgs l)
        {
            if(displayNumber_Label != null)
            {
                if(mathOp == "add")
                {
                    number2 = int.Parse(displayNumber_Label.Text);
                    result = number1 + number2;
                    displayNumber_Label.Text = result.ToString();
                }

                else if(mathOp == "subtract")
                {
                    number2 = int.Parse(displayNumber_Label.Text);
                    result = number1 - number2;
                    displayNumber_Label.Text = result.ToString();
                }

                else if(mathOp == "multiply")
                {
                    number2 = int.Parse(displayNumber_Label.Text);
                    result = number1 * number2;
                    displayNumber_Label.Text = result.ToString();
                }

                else if(mathOp == "divide")
                {
                    number2 = int.Parse(displayNumber_Label.Text);
                    result = number1 / number2;
                    displayNumber_Label.Text = result.ToString();
                }

                number1 = 0;
                number2 = 0;
                result = 0;
            }
        }
    }
        
}
